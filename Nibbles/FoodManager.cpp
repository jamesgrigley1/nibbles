#include "stdafx.h"
#include "Utility.cpp"

class FoodManager
{
public:
	int foodX;
	int foodY;
	float foodSize = 30;

	FoodManager(){
		foodX = 0;
		foodY = 0;
	}

	void placeRandomFood(Utility::rect boundry){
		foodX = Utility::getRandomInt(boundry.x, boundry.x + boundry.w - foodSize);
		foodY = Utility::getRandomInt(boundry.y + foodSize, boundry.y + boundry.h - foodSize);
	}

	void drawRect() {
		
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);
		glVertex2f(foodX, foodY);
		glVertex2f(foodX + foodSize, foodY);
		glVertex2f(foodX + foodSize, foodY + foodSize);
		glVertex2f(foodX, foodY + foodSize);
		glEnd();
	}

	void updateFoodManager(){

	}

	void drawFoodManager(){
		drawRect();
	}
};