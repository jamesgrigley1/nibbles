#include "stdafx.h"
#include "PathManager.cpp";
#include "Utility.cpp"

class SnakeBit{

public:
	float bitSpeed = 0;
	int bitSize = 10;
	PathManager * pathManager = new PathManager();

	float x = 0;
	float y = 0;
	int position;
	int direction = 0; //0=stop, 1=North, 2=East, 3=south, 4=west


	SnakeBit(int _position, int _bitSpeed)
	{
		position = _position;
		bitSpeed = _bitSpeed;
	}

	~SnakeBit()	{}

	bool isActive(){
		return x > 0 && y > 0;
	}

	void setPosition(float _x, float _y){

		x = _x;
		y = _y;

	}
	//UPDATE
	void updateSnakeBit(PathManager::bitPath newBitPath){
		if (isActive()){
			if (position > 0)
				checkBitPaths(newBitPath);
			moveSnakeBit();
		}
	}

	//DRAW
	void drawSnakeBit(){
		if (isActive())
			Utility::drawRect(x, y, bitSize, bitSize, 1, 0, 0);
	}

private:

	void checkBitPaths(PathManager::bitPath newBitPath)
	{
		direction = pathManager->checkPath(newBitPath, x, y, direction);
	}

	void moveSnakeBit(){
		float moveX = 0;
		float moveY = 0;

		if (direction == 1){ //up
			moveY += bitSpeed;
		}
		else if (direction == 2){//right
			moveX += bitSpeed;
		}
		else if (direction == 3){//down
			moveY -= bitSpeed;
		}
		else if (direction == 4){//left
			moveX -= bitSpeed;
		}

		x += moveX;
		y += moveY;
	}

};