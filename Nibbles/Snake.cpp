#include "stdafx.h"
#include "snakeBit.cpp"
#include "PathManager.cpp";

class Snake{
private:
	bool isDead = false;
public:
	int maxNumberOfBits = 100;
	int numberOfBits = 20;
	int direction; //0=stop, 1=North, 2=East, 3=south, 4=west
	float speed = 1;
	int sizeOfSnakeBit = 10;
	
	SnakeBit * snakeBits[100];

	Snake()
	{
		direction = 0;
		//snakeBits[100];
		for (int x = 0; x < maxNumberOfBits; x++){
			snakeBits[x] = new SnakeBit(x, speed);
			if (x < numberOfBits){
				snakeBits[x]->setPosition(1000 - (x * (sizeOfSnakeBit)), 300);
			}
		}
		
	}

	void changeDirection(int newDirection){
		if (direction == 0 && newDirection == 4){
			//do nothing
		}
		else if (direction == 0)
			direction = 2;
		else if (newDirection == 1 && direction != 3)
			direction = 1;
		else if (newDirection == 2 && direction != 4)
			direction = 2;
		else if (newDirection == 3 && direction != 1)
			direction = 3;
		else if (newDirection == 4 && direction != 2)
			direction = 4;
	}

	SnakeBit getLeadSnakeBit(){
		return * snakeBits[0];
	}

	void grow(){
		double lastX, lastY;
		int lastDirection;
		PathManager::bitPath * lastBitPaths[10];

		for (int x = 1; x < maxNumberOfBits; x++){
			if (snakeBits[x]->isActive() == false){
				snakeBits[x]->direction = lastDirection;
				for (int i = 0; i < 10; i++){
					*snakeBits[x]->pathManager->nextBitPath[i] = *lastBitPaths[i];
				}
				if (lastDirection == 1){
					snakeBits[x]->setPosition(lastX, lastY - sizeOfSnakeBit);
				}else if (lastDirection == 2){
					snakeBits[x]->setPosition(lastX - sizeOfSnakeBit, lastY);
				}
				else if (lastDirection == 3){
					snakeBits[x]->setPosition(lastX, lastY + sizeOfSnakeBit);
				}
				else if (lastDirection == 4){
					snakeBits[x]->setPosition(lastX + sizeOfSnakeBit, lastY);
				}

				
				break;
				
			}
			else{
				lastX = snakeBits[x]->x;
				lastY = snakeBits[x]->y;
				lastDirection = snakeBits[x]->direction;
				for (int i = 0; i < 10; i++){
					lastBitPaths[i] = snakeBits[x]->pathManager->nextBitPath[i];
				}
			}
		}
	}

	void moveSnake(){
		
		PathManager::bitPath bitPath;
		bitPath.x = 0;
		bitPath.y = 0;
		bitPath.direction = 0;

		// get lead snake bit's direction
		int currentDirection = snakeBits[0]->direction;

		// if this is a change in direction
		if (direction > 0 && snakeBits[0]->direction > 0 && direction != currentDirection){
			// then, we want to set a path
			bitPath.x = snakeBits[0]->x;
			bitPath.y = snakeBits[0]->y;
			bitPath.direction = direction;
		}

		snakeBits[0]->direction = direction;

		for (int i = 0; i < maxNumberOfBits; i++){
			if (snakeBits[i]->isActive())
			{
				// if snake bit doesn't have a direction, give it the snakes direction
				if (snakeBits[i]->direction == 0)
					snakeBits[i]->direction = direction;
			}
			snakeBits[i]->updateSnakeBit(bitPath);
		}
	}

	bool checkSnakeSelfCollision(){
		SnakeBit * leadSnakeBit = snakeBits[0];
		SnakeBit * followSnakeBit;

		for (int i = 2; i < maxNumberOfBits; i++){
			if (snakeBits[i]->isActive())
			{
				followSnakeBit = snakeBits[i];
				if (
					leadSnakeBit->x > followSnakeBit->x 
					&& leadSnakeBit->x < followSnakeBit->x+sizeOfSnakeBit
					&& leadSnakeBit->y > followSnakeBit->y
					&& leadSnakeBit->y < followSnakeBit->y + sizeOfSnakeBit
					)
				{
					return true;
				}
			}

		}
		return false;
	}

	//UPDATE
	void updateSnake(){
		if (direction > 0)
		{
			moveSnake();
		}
	}
	//DRAW
	void drawSnake(){
		if (isDead)
			return;

		for (int x = 0; x < maxNumberOfBits; x++){
			if (snakeBits[x]->isActive())
				snakeBits[x] -> drawSnakeBit();
		}
	}
	
	void die(){
		isDead = true;
		direction = 0;
	}

	~Snake()
	{
	}
};
