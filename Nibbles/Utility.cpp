#include "stdafx.h"
#include <random>
#pragma once

class Utility{

public:
	static int getRandomInt(int lowerRange, int upperRange){
		std::default_random_engine generator((std::random_device())()); // create and seed the source of randomness
		std::uniform_int_distribution<int> distribution(lowerRange, upperRange); 
		return distribution(generator);  // generates number in the range 
	}

	struct rect
	{
		int x, y, w, h;
	};

	static bool checkCollision(rect rec1, rect rec2){

	}

	static void drawRect(int x, int y, int w, int h, float r, float g, float b) {
		glColor3f(r, g, b);
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + w, y);
		glVertex2f(x + w, y + h);
		glVertex2f(x, y + h);
		glEnd();

		//drawText(x + 2, y + 2, int2str(position));
	}


	static void drawText(float x, float y, std::string text, int size, float r, float g, float b) {
		void *font;

		if (size == 1)
		{
			font = GLUT_BITMAP_HELVETICA_10;
		}
		else if (size == 2)
		{
			font = GLUT_BITMAP_HELVETICA_12;
		}
		else if (size >= 3)
		{
			font = GLUT_BITMAP_HELVETICA_18;
		}
		else
			return;

		glColor3f(r, g, b);
		glRasterPos2f(x, y);
		glutBitmapString(font, (const unsigned char*)text.c_str());
	}

	static std::string int2str(int x) {
		// converts int to string
		std::stringstream ss;
		ss << x;
		return ss.str();
	}
};