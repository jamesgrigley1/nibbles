#include "stdafx.h"
#include "snake.cpp"
#include "FoodManager.cpp";
#include "BoundryManager.cpp";
#include "HUD.cpp";

#pragma comment(lib, "Opengl32.lib")
#define VK_A 0x41
#define VK_D 0x44
#define VK_S 0x53
#define VK_W 0x57
#define VK_SPACE 0x20

// window size and update rate (60 fps)
int width = 1600;
int height = 900;
int interval = 1000 / 60;
bool gameOver = false;

Snake * snake = new Snake();
FoodManager * foodManager = new FoodManager();
BoundryManager * boundryManager = new BoundryManager(300, 100, 1200, 700);
HUD *hud = new HUD();

void newGame(){
	snake = new Snake();
	gameOver = false;
	foodManager->placeRandomFood(boundryManager->getBoundryRect());
	hud = new HUD();
}

void keyboard() {
	if (GetAsyncKeyState(VK_W)) 
		snake->changeDirection(1);
	if (GetAsyncKeyState(VK_D)) 
		snake->changeDirection(2);
	if (GetAsyncKeyState(VK_S)) 
		snake->changeDirection(3);
	if (GetAsyncKeyState(VK_A)) 
		snake->changeDirection(4);

	if (gameOver && GetAsyncKeyState(VK_SPACE))
	{
		newGame();
	}
	//int c = _getch();
	//if (c)
	//	Utility::drawText(400, 400, "Key Is Pressed", 3, 1, 0, 0);
}

bool checkSnakeAteFood(){
	SnakeBit snakeBit = snake->getLeadSnakeBit();
	
	if (snakeBit.x+10 >= foodManager->foodX && snakeBit.y+10 >= foodManager->foodY && snakeBit.x <= (foodManager->foodX + 30) && snakeBit.y <= (foodManager->foodY + 30))
	{
		// snake/food collision
		return true;
	}
	else{
		return false;
	}
}

bool checkSnakeHitWall()
{
	SnakeBit snakeBit = snake->getLeadSnakeBit();
	Utility::rect boundry = boundryManager->getBoundryRect();

	if (
		snakeBit.x < boundry.x || // left
		snakeBit.x + 10 > boundry.x + boundry.w || //right
		snakeBit.y < boundry.y || //bottom
		snakeBit.y + 10 > boundry.y + boundry.h
		)
		return true;
	else
		return false;

}
void showGameOver(){
	Utility::drawText(400, 400, "Game Over! You are suck! Press space bar to continue.", 3, 1, 0, 0);
}


void start(){
	newGame();
}
//UPDATE =========================================
void update(int i){
	// do updates here
	// input handling
	keyboard();

	snake->updateSnake();
	if (checkSnakeAteFood()){
		hud->addToScore(10);
		hud->foodLeft -= 1;
		foodManager->placeRandomFood(boundryManager->getBoundryRect());
		snake->grow();
	}
	foodManager->updateFoodManager();

	if (checkSnakeHitWall() || snake->checkSnakeSelfCollision())
	{
		snake->die();
		gameOver = true;
	}

	hud->updateHUD();

	// Call update() again in 'interval' milliseconds
	glutTimerFunc(interval, update, 0);
	
	// Redisplay frame
	glutPostRedisplay();
}

//DRAW =========================================
void draw(){
	// clear (has to be done at the beginning)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	if (gameOver){
		showGameOver();
	}
	else{
		snake->drawSnake();
	}
	foodManager->drawFoodManager();
	boundryManager->drawBoundry();
	hud->drawHUD();

	glutSwapBuffers();
}
void enable2D(int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, width, 0.0f, height, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// program entry point
int _tmain(int argc, char** argv) {

	// initialize opengl (via glut)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("Nibbles");

	// Register callback functions  
	start();
	glutDisplayFunc(draw);
	glutTimerFunc(interval, update, 0);

	// setup scene to 2d mode and set draw color to white
	enable2D(width, height);
	glColor3f(1.0f, 0.0f, 1.0f);

	// start the whole thing
	glutMainLoop();
	return 0;
}
