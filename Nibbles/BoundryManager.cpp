#include "stdafx.h"
#include "Utility.cpp"

class BoundryManager
{
public:
	int borderWidth = 10;
	int x, y, w, h;

	BoundryManager(int _x, int _y, int _w, int _h)
	{
		x = _x;
		y = _y;
		w = _w;
		h = _h;
	}

	void updateBoundry(){}

	void drawBoundry(){
		Utility::drawRect(x - borderWidth, y + h, w + (borderWidth*2), borderWidth, 1.0f, 1.0f, 1.0f); // top
		Utility::drawRect(x, y, w, borderWidth, 1.0f, 1.0f, 1.0f); // bottom
		Utility::drawRect(x - borderWidth, y, borderWidth, h, 1.0f, 1.0f, 1.0f); // left
		Utility::drawRect(w + x, y, borderWidth, h, 1.0f, 1.0f, 1.0f); // right
	}

	Utility::rect getBoundryRect(){
		Utility::rect *rect = new Utility::rect;
		rect->x = x;
		rect->y = y;
		rect->w = w;
		rect->h = h;
		return *rect;
	}

};