#include "stdafx.h"
#pragma once
class PathManager{
private:
	int maxPaths = 50;

	//

public:

	struct bitPath{
		float x;
		float y;
		int direction;
	};

	bitPath * nextBitPath[50];

	void addBitPath(bitPath newBitPath){
		for (int i = 0; i <maxPaths; i++){
			if (nextBitPath[i]->direction == 0){
				nextBitPath[i]->direction = newBitPath.direction;
				nextBitPath[i]->x = newBitPath.x;
				nextBitPath[i]->y = newBitPath.y;
				return;
			}
		}
	}

	void removeFirstBitPath()
	{
		for (int i = 0; i<maxPaths-1; i++){
			nextBitPath[i]->x = nextBitPath[i + 1]->x;
			nextBitPath[i]->y = nextBitPath[i + 1]->y;
			nextBitPath[i]->direction = nextBitPath[i + 1]->direction;
		}

		nextBitPath[maxPaths-1]->x = 0;
		nextBitPath[maxPaths-1]->y = 0;
		nextBitPath[maxPaths-1]->direction = 0;
	}

	int checkPath(bitPath newBitPath, int x, int y, int direction){
		if (newBitPath.direction > 0){
			addBitPath(newBitPath);
		}

		// we will not loop, only checking first path
		if (nextBitPath[0]->direction > 0){
			// if we have reached or exceeded our next path
			if ((direction == 1 && y >= nextBitPath[0]->y) ||
				(direction == 2 && x >= nextBitPath[0]->x) ||
				(direction == 3 && y <= nextBitPath[0]->y) ||
				(direction == 4 && x <= nextBitPath[0]->x)
				)
			{
				direction = nextBitPath[0]->direction;
				removeFirstBitPath();
			}
		}	
		return direction;
	}

	PathManager(){
		for (int i = 0; i < maxPaths; i++){
			nextBitPath[i] = new bitPath;
			nextBitPath[i]->x = 0;
			nextBitPath[i]->y = 0;
			nextBitPath[i]->direction = 0;
		}
	}
	
};